#include <stdio.h>
#include <stdbool.h>

/**
 * Affiche quatre phrases de façon cyclique.
 *
 * Les phrases affichées sont
 *
 * * un peu
 * * beaucoup
 * * passionnément
 * * à la folie
 * * pas du tout
 */
void aimer_cycle(void) {
    printf("un peu\n");
}

/**
 * Affiche quatre phrases de façon palindromique.
 *
 * Les phrases affichées sont
 *
 * * un peu
 * * beaucoup
 * * passionnément
 * * à la folie
 * * pas du tout
 */
void aimer_palindrome(void) {
    printf("un peu\n");
}

int main(void) {
    printf("Cycle:\n");
    for (unsigned int i = 0; i < 20; ++i) {
        printf("%.2d: ", i);
        aimer_cycle();
    }
    printf("Palindrome:\n");
    for (unsigned int i = 0; i < 20; ++i) {
        printf("%.2d: ", i);
        aimer_palindrome();
    }
    printf("\n");
    return 0;
}
