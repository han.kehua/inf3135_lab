# Labo 12 : Intégration

L'objectif de ce laboratoire est de se familiariser avec le suivi des problèmes
(*issues*), le développement à l'aide branches et les requêtes d'intégration
(*merge requests*).

Le laboratoire est organisé pour être réalisé en équipe de deux. Si vous
travaillez seul, essayer d'adapter le laboratoire pour un développement en
solitaire.  Lire complètement chaque partie avant de la commencer.

## 1 - Récupération du programme (20 minutes)

* Choisir un coéquipier ou une coéquipière
* Prendre contact en privé sur Mattermost avec l'autre (ça facilitera le
  déroulement du laboratoire)
* S'entendre sur qui jouera le rôle d'Alice et qui jouera le rôle de Bob (les
  rôles sont assez similaires)
* Les instructions seront précédées de @Alice, @Bob ou @Alice/Bob selon
  qu'elles s'adressent à Alice, à Bob ou aux deux personnes en même temps
* **@Alice**: dupliquer (*fork*) le dépôt public
  [connect4-part2](https://gitlab.info.uqam.ca/inf3135-sdo/connect4-part2)
* **@Alice**: laisser la visibilité *publique* (pour que les démonstrateurs
  puissent aider)
* **@Alice**: donner accès au projet à Bob en mode *Maintainer*
* **@Bob**: confirmer à Alice que l'accès est bien accordé
* **@Alice/Bob**: cloner en local le dépôt d'Alice
* **@Alice/Bob**: étudier le contenu du projet, en faisant des tests
  d'exécution, en consultant le code source, le Makefile disponible, les tests,
  etc.

## 2 - Identification des problèmes (20 minutes)

* **@Alice**: lire le problème
  [#1](https://gitlab.info.uqam.ca/inf3135-sdo/connect4-part2/-/issues/1)
* **@Bob**: lire le problème
  [#2](https://gitlab.info.uqam.ca/inf3135-sdo/connect4-part2/-/issues/2)
* **@Alice/@Bob**: avant de commencer à résoudre vos problèmes respectifs,
  faire le point sur vos tâches respectives. Résumer brièvement chacun de vos
  problèmes l'un à l'autre, en maximum 5 minutes par personne (soit à l'oral,
  soit par clavardage). Poser des questions pour clarifier certains points si
  nécessaire. Indiquer brièvement quelle stratégie est prévue pour résoudre
  chacun des problèmes.

## 3 - Résolution des problèmes (40 minutes)

* **@Alice**: créer une branche nommée `alternate` et proposer des
  modifications permettant de résoudre le problème `#1`. Répartir en petits
  *commits*, ayant des messages significatifs et de syntaxe uniforme, cohérente
  avec celle déjà utilisée dans le dépôt.
* **@Bob**: créer une branche nommée `usage` et proposer des modifications
  permettant de résoudre le problème `#2`. Répartir en petits *commits*, ayant
  des messages significatifs et de syntaxe uniforme, cohérente avec celle déjà
  utilisée dans le dépôt.
* **@Alice/@Bob**: au besoin, discuter entre vous pour faire état de votre
  avancement, des difficultés rencontrées. Poser des questions si nécessaire.
* **@Alice/@Bob**: préparer une requête d'intégration qui présente les
  modifications apportées par vos branches respectives vers la branche
  `master`. Écrire une requête la plus claire possible pour que l'autre
  personne comprenne la problématique le plus rapidement possible. Donner des
  exemples d'exécution autant que possible, c'est plus visuel et plus facile à
  comprendre qu'un long texte détaillé. Aussi, ajouter une référence au
  problème (*issue*) depuis la requête d'intégration pour indiquer que vous la
  résolvez (il suffit d'utiliser `#n`, où `n` est le numéro de l'*issue*). De
  façon typique, on met une phrase du type: `Résoud le problème #n`.
* **@Alice**: assigner la requête d'intégration à Bob
* **@Bob**: assigner la requête d'intégration à Alice

## 4 - Révision des requêtes d'intégration (20 minutes)

* **@Alice**: réviser la requête de Bob en apportant des commentaires.
  Récupérer la branche localement avec `git fetch origin` suivi de `git
  checkout usage`
* **@Bob**: réviser la requête d'Alice en apportant des commentaires. Récupérer
  la branche localement avec `git fetch origin` suivi de `git checkout
  alternate`
* Éléments à vérifier:

    * Qualité des *commits*: spécifiques, message significatif, syntaxe
      uniforme, pas de faute
    * Fonctionnalité: est-ce que la requête résoud tous les problèmes?
    * Qualité des modifications: style du code, espaces superflus, aération,
      indentation, etc.
    * Au besoin, apportez des modifications à celles de l'autre, en ajoutant
      des *commits* directement sur la branche

* **@Alice**: consulter les suggestions de Bob et rectifier en ajoutant des
  *commits* supplémentaires sur la branche. En cas de désaccord, expliquer
  brièvement pourquoi, en utilisant des arguments objectifs.
* **@Bob**: consulter les suggestions d'Alice et rectifier en ajoutant des
  *commits* supplémentaires sur la branche. En cas de désaccord, expliquer
  brièvement pourquoi, en utilisant des arguments objectifs.

## 5 - Intégration (10 minutes)

* **@Alice**: vérifier une dernière fois la requête d'intégration de Bob et
  intégrer la branche à l'aide du bouton *Merge* sur l'interface web de GitLab,
  dans la requête d'intégration
* **@Bob**: vérifier une dernière fois la requête d'intégration d'Alice et
  intégrer la branche à l'aide du bouton *Merge* sur l'interface web de GitLab,
  dans la requête d'intégration
* **@Alice/Bob**: synchroniser vos dépôts respectifs sur la branche `master` et
  vérifier que le programme fonctionne correctement et que les modifications
  ont bien été intégrées.
* **@Alice/Bob**: vous féliciter mutuellement pour votre travail d'équipe!
