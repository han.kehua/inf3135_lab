#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// Types
// -----

struct tree_node {
    char *key;               // Key
    char *value;             // Value
    struct tree_node *left;  // Left subtree
    struct tree_node *right; // Right subtree
};

typedef struct {
    struct tree_node *root; // Root of tree
} treemap;

// Interface
// ---------

/**
 * Initialize an empty tree map
 *
 * @param t  The tree map to initialize
 */
void treemap_initialize(treemap *t);

/**
 * Return the value associated with the given key in a tree map
 *
 * Note: if the key does not exist, return NULL.
 *
 * @param t    The tree map
 * @param key  The key to search
 */
char *treemap_get(const treemap *t, const char *key);

/**
 * Set the value for the given key in a tree map
 *
 * @param t      The tree map
 * @param key    The key
 * @param value  The value
 */
void treemap_set(treemap *t, const char *key, const char *value);

/**
 * Indicate if a key exists in a tree map
 *
 * @param t    The tree map
 * @param key  The key
 * @return     True if and only if the key exists in the tree map
 */
bool treemap_has_key(const treemap *t, const char *key);

/**
 * Print a tree map to stdout
 *
 * @param t  The tree map to print
 */
void treemap_print(const treemap *t);

/**
 * Delete a tree map
 *
 * @param t  The tree map to delete
 */
void treemap_delete(treemap *t);

// Auxiliary functions
// -------------------

/**
 * Return the node associated with a given key in a tree map
 *
 * Note: if the key does not exist in the tree map, return NULL.
 *
 * @param node  The current node
 * @param key   The key to search
 * @return      The node associated with the key
 */
struct tree_node *treemap_get_node(struct tree_node *node,
                                   const char *key);

/**
 * Insert a key-value node in a tree map
 *
 * @param node   The current node
 * @param key    The key to insert
 * @param value  The value to insert
 */
void treemap_insert_node(struct tree_node **node,
                         const char *key,
                         const char *value);

/**
 * Print the tree map starting from the given node
 *
 * @param node  The current node
 */
void treemap_print_node(const struct tree_node *node);

/**
 * Delete all nodes referenced by the given node
 *
 * @param node  The current node
 */
void treemap_delete_node(struct tree_node *node);

// Implementation
// --------------

void treemap_initialize(treemap *t) {
    t->root = NULL;
}

char *treemap_get(const treemap *t, const char *key) {
    struct tree_node *node = treemap_get_node(t->root, key);
    return node == NULL ? NULL : node->value;
}

struct tree_node *treemap_get_node(struct tree_node *node,
                                   const char *key) {
    if (node == NULL) {
        return NULL;
    } else {
        int cmp = strcmp(key, node->key);
        if (cmp == 0)
            return (struct tree_node*)node;
        else if (cmp < 0)
            return treemap_get_node(node->left, key);
        else
            return treemap_get_node(node->right, key);
    }
}

void treemap_set(treemap *t, const char *key, const char *value) {
    struct tree_node *node = treemap_get_node(t->root, key);
    if (node != NULL) {
        free(node->value);
        node->value = strdup(value);
    } else {
        treemap_insert_node(&(t->root), key, value);
    }
}

void treemap_insert_node(struct tree_node **node,
                         const char *key,
                         const char *value) {
    if (*node == NULL) {
        *node = malloc(sizeof(struct tree_node));
        (*node)->key = strdup(key);
        (*node)->value = strdup(value);
        (*node)->left = NULL;
        (*node)->right = NULL;
    } else if (strcmp(key, (*node)->key) < 0) {
        treemap_insert_node(&(*node)->left, key, value);
    } else {
        treemap_insert_node(&(*node)->right, key, value);
    }
}

bool treemap_has_key(const treemap *t, const char *key) {
    return treemap_get_node(t->root, key) != NULL;
}

void treemap_print(const treemap *t) {
    printf("TreeMap {\n");
    treemap_print_node(t->root);
    printf("}\n");
}

void treemap_print_node(const struct tree_node *node) {
    if (node != NULL) {
        treemap_print_node(node->left);
        printf("  %s: %s\n", node->key, node->value);
        treemap_print_node(node->right);
    }
}

void treemap_delete(treemap *t) {
    treemap_delete_node(t->root);
}

void treemap_delete_node(struct tree_node *node) {
    if (node != NULL) {
        treemap_delete_node(node->left);
        treemap_delete_node(node->right);
        free(node->key);
        free(node->value);
        free(node);
    }
}

// Main
// ----
int main() {
    treemap t;
    treemap_initialize(&t);
    treemap_set(&t, "firstname", "Doina");
    treemap_set(&t, "lastname", "Precup");
    treemap_set(&t, "city", "Montreal");
    treemap_set(&t, "province", "Quebec");
    treemap_set(&t, "country", "Canada");
    treemap_set(&t, "position", "DeepMind Montreal Head");
    printf("Printing the tree map\n");
    treemap_print(&t);
    printf("Get \"firstname\": %s\n", treemap_get(&t, "firstname"));
    printf("Get \"province\": %s\n", treemap_get(&t, "province"));
    printf("Get \"position\": %s\n", treemap_get(&t, "position"));
    printf("Changing country to Romania\n");
    treemap_set(&t, "country", "Romania");
    printf("Get \"country\": %s\n", treemap_get(&t, "country"));
    printf("Printing the tree map\n");
    treemap_print(&t);
    treemap_delete(&t);
}
